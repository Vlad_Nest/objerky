﻿using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;
using Objerky.Models;

namespace Objerky.Providers
{
    public class TokenAuthorisationServerProvider:OAuthAuthorizationServerProvider
    {
        private string _publicClientId;
        public string PublicClientId { get { return _publicClientId; } private set { _publicClientId = value; } }
        public TokenAuthorisationServerProvider(string name)
        {
            PublicClientId = name;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            return Task.Factory.StartNew(() => context.Validated());
        }


        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            ApplicationUser user = await userManager.FindByEmailAsync(context.UserName);
            bool PassCorrect = await userManager.CheckPasswordAsync(user, context.Password);
            if (user == null || !PassCorrect)
            {
                context.SetError("invalid_grant", "Имя пользователя или пароль указаны неправильно.");
                return;
            } 

            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager);

            AuthenticationProperties properties = CreateProperties(user.UserName);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(oAuthIdentity);
        }

        public static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }
    }
}