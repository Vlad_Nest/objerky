﻿using System;
using System.Collections.Generic;
using System.Linq;
using Objerky.Models;
using Objerky.Models.Repository;
using Objerky.Models.Data;

namespace Objerky.Models.Service
{
    public interface IItemService
    {
        IEnumerable<Item> GetItems();
        IEnumerable<Item> GetCategoryItems(int categoryid);
        Item GetItem(int id);
        void CreateItem(Item item);
        void SaveItem();
    }
    public class ItemService:IItemService
    {
        private readonly IItemRepository itemRep;
        private readonly IUnitOfWork unitOfWork;

        public ItemService(IItemRepository itemRep, IUnitOfWork unitOfWork)
        {
            this.itemRep = itemRep;
            this.unitOfWork = unitOfWork;
        }

        #region IItemService Members

        public IEnumerable<Item> GetItems()
        {
            var items = itemRep.GetAll();
            return items;  
        }
        
        public IEnumerable<Item> GetCategoryItems(int category)
        {
            var items = itemRep.GetMany(x => x.Category.Id == category);
            return items;
        }

        public Item GetItem(int id)
        {
            var item = itemRep.GetById(id);
            return item;
        }

        public void CreateItem(Item item)
        {
            itemRep.Add(item);
        }

        public void SaveItem()
        {
            unitOfWork.Commit();
        }
        #endregion
    }
}