﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Objerky.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual ICollection<Item> Items { get; set; }
    }
}