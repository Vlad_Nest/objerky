﻿using System;
using System.Collections.Generic;
using System.Linq;
using Objerky.Models;
using Objerky.Models.Data;

namespace Objerky.Models.Repository
{
    public interface IItemRepository:IRepository<Item>
    {

    }
    public class ItemRepository:RepositoryBase<Item>, IItemRepository
    {
        public ItemRepository(DbFactory factory) : base(factory) { }
    }
}