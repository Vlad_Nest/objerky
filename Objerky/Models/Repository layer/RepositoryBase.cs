﻿using System;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Objerky.Models.Data;

namespace Objerky.Models.Repository
{
    public abstract class RepositoryBase<T> where T : class
    {
        #region Properties
        private ApplicationDbContext dataContext;
        private readonly IDbSet<T> dbSet;

        protected IDBFactory DbFactory
        {
            get;
            private set;
        }

        protected ApplicationDbContext DbContext
        {
            get { return dataContext ?? (dataContext = DbFactory.Init()); }
        }
        #endregion

        protected RepositoryBase(IDBFactory factory)
        {
            DbFactory = factory;
            dbSet = DbContext.Set<T>();
        }

        #region Implementation

        public virtual T GetById(int id)
        {
            return dbSet.Find(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T,bool>> where)
        {
            return dbSet.Where(where).ToList();
        }

        public virtual T Get(Expression<Func<T,bool>> where)
        {
            return dbSet.Where(where).FirstOrDefault<T>();
        }
        
        public virtual void Add(T entity)
        {
            dbSet.Add(entity);
        }
        #endregion
    }
}