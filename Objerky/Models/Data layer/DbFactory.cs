﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Objerky.Models.Data
{
    public interface IDBFactory : IDisposable
    {
        ApplicationDbContext Init();
    }
    public class DbFactory: Disposable, IDBFactory
    {
        ApplicationDbContext dbContext;

        public ApplicationDbContext Init()
        {
            return dbContext ?? (dbContext = new ApplicationDbContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}