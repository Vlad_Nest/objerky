﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Objerky.Models.Data
{
    public interface IUnitOfWork
    {
        void Commit();
    }
    public class UnitOfWork: IUnitOfWork
    {
        private readonly IDBFactory dbFactory;
        private ApplicationDbContext dbContext;
        public UnitOfWork(IDBFactory factory)
        {
            this.dbFactory = factory;
        }
        public ApplicationDbContext DbContext
        {
            get { return dbContext ?? (dbContext = dbFactory.Init()); }
        }

        public void Commit()
        {
            DbContext.Commit();
        }
    }
}