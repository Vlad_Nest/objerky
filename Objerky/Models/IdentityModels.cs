﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Objerky.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Objerky.Models.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Item> Items { get; set; }
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public virtual void Commit()
        {
            base.SaveChanges();
        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>()
                .HasMany(x => x.Components).WithRequired().HasForeignKey(x => x.ItemId);
            modelBuilder.Entity<Composition>()
                .HasKey(x => new { x.Id, x.ItemId })
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Item>()
                .HasRequired<Category>(x => x.Category)
                .WithMany(x => x.Items);
            base.OnModelCreating(modelBuilder);
        }
    }
}