﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Objerky.Models
{
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual byte[] Image { get; set; }
        public virtual string Description { get; set; }
        public virtual int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
        public virtual ICollection<Composition> Components{ get; set; }
    }

    public class Composition
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public virtual int Amount { get; set; }
        public virtual CompositionType Type { get; set; }
    }

    public enum CompositionType
    {
        Pork = 0,
        Beef = 1
    }
}