﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Objerky.Startup))]
namespace Objerky
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
